class AircraftImportWorker
  include Sidekiq::Worker

  def perform(*args)
    Aircraft.request_aircraft(args[0])
  end
end
