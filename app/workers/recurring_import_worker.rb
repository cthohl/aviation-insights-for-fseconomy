class RecurringImportWorker
  include Sidekiq::Worker

  def perform(*args)
    raise RuntimeError, 'No aircraft types found.' unless AircraftType.count > 0
    aircraft_types = AircraftType
                     .order(aircraft_fetched_at: 'desc')
                     .limit(4)
    AircraftImportWorker.perform_async(aircraft_types[0].make_model)
    AircraftImportWorker.perform_at(15.seconds.from_now, aircraft_types[1].make_model)
    AircraftImportWorker.perform_at(30.seconds.from_now, aircraft_types[2].make_model)
    AircraftImportWorker.perform_at(45.seconds.from_now, aircraft_types[3].make_model)

    raise RuntimeError, 'No airports found.' unless Airport.count > 0
    airport_ids = Airport
                  .order('jobs_last_fetched ASC NULLS FIRST')
                  .limit(1000)
                  .ids
    JobImportWorker.perform_at(10.seconds.from_now, airport_ids)

    RecurringImportWorker.perform_at(60.seconds.from_now)
  end
end
