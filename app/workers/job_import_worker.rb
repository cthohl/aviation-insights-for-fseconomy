# frozen_string_literal: true

class JobImportWorker
  include Sidekiq::Worker

  def perform(*args)
    Job.refresh_jobs_batch(args[0])
  end
end
