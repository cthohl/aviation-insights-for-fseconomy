import CssBaseline from '@material-ui/core/CssBaseline';
import Layout from '../layout';
import React from 'react';
import ReactDOM from 'react-dom';

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <div>
      <CssBaseline />
      <Layout />
    </div>,
    document.getElementById('vals_app'),
  )
})

