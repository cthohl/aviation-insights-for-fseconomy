import { AircraftSelect } from './aircraft_select';
import AppBar from '@material-ui/core/AppBar';
import FlightTable from './flight_table';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import { IntlProvider } from 'react-intl';
import MenuIcon from '@material-ui/icons/Menu';
import { Provider } from 'react-redux';
import React from 'react';
import store from './store';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

export default class Layout extends React.Component {
  render () {
    return (
      <Provider store={store}>
        <IntlProvider locale="en">
          <Grid container spacing={16}>
            <AppBar position="static">
              <Toolbar>
                <Typography variant="title" color="inherit" style={{flex: 1}}>
                  Aviation Insights for FSEconomy
                </Typography>
                <IconButton color="inherit" aria-label="Menu">
                  <MenuIcon />
                </IconButton>
              </Toolbar>
            </AppBar>
            <Grid item xs={12}>
              <Grid container justify="center">
                <Grid item>
                  <Grid container justify="flex-end" spacing={16}>
                    <Grid item style={{marginBottom: 16}}>
                      <AircraftSelect />
                    </Grid>
                  </Grid>
                  <FlightTable />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </IntlProvider>
      </Provider>
    );
  }
}

store.subscribe(() => {
  console.log('store changed', store.getState());
});

