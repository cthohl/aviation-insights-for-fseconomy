export default (state, action) => {
  if (action.type === 'SELECT_AIRCRAFT') {
    return {
      ...state,
      selectedAircraft: action.payload
    };
  }

  if (action.type === 'CHANGE_COLUMN_SORT') {
    return {
      ...state,
      direction: action.payload.direction,
      orderBy: action.payload.orderBy
    };
  }

  if (action.type === 'FETCH_FLIGHTS_FULFILLED') {
    return {
      ...state,
      flights: action.payload.flights,
    };
  }

  if (action.type === 'FETCH_AIRCRAFT_TYPES_FULFILLED') {
    const first_aircraft_type = action.payload.aircraft_types.slice().shift();

    return {
      ...state,
      aircraftTypes: action.payload.aircraft_types,
      selectedAircraft: first_aircraft_type && first_aircraft_type.make_model
    };
  }

  return state;
};

