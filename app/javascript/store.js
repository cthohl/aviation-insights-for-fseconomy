import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';

const initialState = {
  aircraftTypes: [],
  direction: 'desc',
  flights: [],
  orderBy: 'revenue',
  selectedAircraft: ''
};

const middleware = [ thunk ];

const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({ /* options here */ }) : compose;

const enhancers = composeEnhancers(
  applyMiddleware(...middleware)
);

export default createStore(
  reducers,
  initialState,
  enhancers
);

