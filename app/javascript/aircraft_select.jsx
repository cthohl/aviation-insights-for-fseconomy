import { connect } from 'react-redux';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import React from 'react';
import Select from '@material-ui/core/Select';
import { fetchAircraftTypes, fetchFlights, selectAircraft } from './actions';

@connect((store) => {
  return {
    aircraftTypes: store.aircraftTypes,
    direction: store.direction,
    orderBy: store.orderBy,
    selectedAircraft: store.selectedAircraft
  };
})
export class AircraftSelect extends React.Component {
  componentDidMount() {
    this.props.dispatch(fetchAircraftTypes());
  }

  handleChange = name => event => {
    this.props.dispatch(selectAircraft(event.target.value));
    this.props.dispatch(fetchFlights({
      direction: this.props.direction,
      orderBy: this.props.orderBy,
      selectedAircraft: event.target.value
    }));
  };

  render () {
    const options = this.props.aircraftTypes.map((aircraft) => {
      <option key={aircraft.fse_id} value={aircraft.make_model}>{aircraft.make_model}</option>
    });
    return (
      <FormControl>
        <InputLabel htmlFor="aircraft-select">Aircraft Types</InputLabel>
        <Select
          native
          value={this.props.selectedAircraft}
          onChange={this.handleChange('aircraft')}
          inputProps={{
            name: 'aircraft',
            id: 'aircraft-select',
          }}
        >
          <option value="" />
          {options}
        </Select>
      </FormControl>
    );
  }
}
