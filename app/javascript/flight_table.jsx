// flight_table.jsx
import axios from 'axios';
import { connect } from 'react-redux';
import { changeColumnSort, fetchFlights } from './actions';
import { FormattedNumber } from 'react-intl';
import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import { TablePaginationActionsWrapped } from './table_pagination_actions';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';

@connect((store) => {
  return {
    direction: store.direction,
    flights: store.flights,
    orderBy: store.orderBy,
    selectedAircraft: store.selectedAircraft
  };
})
export default class FlightTable extends React.Component {
  componentDidMount() {
    this.props.dispatch(fetchFlights({
      direction: this.props.direction,
      orderBy: this.props.orderBy,
      selectedAircraft: this.props.selectedAircraft
    }));
  }

  constructor (props) {
    super(props);
    this.state = {
      fseAirportLinkBase: "http://server.fseconomy.net/airport.jsp?icao=",
      fseAircraftLinkBase: "http://server.fseconomy.net/aircraftlog.jsp?id=",
      page: 0,
      rowsPerPage: 10
    };
  }

  render () {
    const { page, rowsPerPage } = this.state;
    const startingIndex = page * rowsPerPage;
    const endBeforeIndex = page * rowsPerPage + rowsPerPage;

    return (
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Location</TableCell>
              <TableCell>Destination</TableCell>
              <TableCell>Distance (NM)</TableCell>
              <TableCell>
                <TableSortLabel
                  active={this.props.orderBy === 'revenue'}
                  direction={this.props.direction}
                  onClick={() => this.handleRequestSort('revenue')}
                >
                  Revenue
                </TableSortLabel>
              </TableCell>
              <TableCell>
                <TableSortLabel
                  active={this.props.orderBy === 'revenue_per_nautical_mile'}
                  direction={this.props.direction}
                  onClick={() => this.handleRequestSort('revenue_per_nautical_mile')}
                >
                  Revenue Per NM
                </TableSortLabel>
              </TableCell>
              <TableCell>Seats Required</TableCell>
              <TableCell>Cargo & Pax Weight (KG)</TableCell>
              <TableCell>Aircraft</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.props.flights.slice(startingIndex, endBeforeIndex).map(flight => {
              return (
                <TableRow key={flight.id}>
                  <TableCell component="th" scope="row">
                    <a href={this.state.fseAirportLinkBase + flight.from_icao} target="_blank">
                      {flight.from_icao}
                    </a>
                  </TableCell>
                  <TableCell>
                    <a href={this.state.fseAirportLinkBase + flight.to_icao} target="_blank">
                      {flight.to_icao}
                    </a>
                  </TableCell>
                  <TableCell>
                    <FormattedNumber value={flight.distance} />
                  </TableCell>
                  <TableCell>
                    <FormattedNumber value={flight.revenue} style="currency" currency="USD" />
                  </TableCell>
                  <TableCell>
                    <FormattedNumber
                      value={flight.revenue_per_nautical_mile}
                      style="currency"
                      currency="USD"
                    />
                  </TableCell>
                  <TableCell>{flight.seats_required}</TableCell>
                  <TableCell>
                    <FormattedNumber value={flight.cargo_and_passenger_weight_kg} />
                  </TableCell>
                  <TableCell>
                    <a href={this.state.fseAircraftLinkBase + flight.aircraft_serial_number} target="_blank">
                      {flight.aircraft_registration}
                    </a>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                ActionsComponent={TablePaginationActionsWrapped}
                colSpan={7}
                count={this.props.flights.length}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
                page={page}
                rowsPerPage={rowsPerPage}
                rowsPerPageOptions={[5, 10, 15]}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </Paper>
    );
  }

  handleChangePage = (event, page) => {
    this.setState({ page });
  }

  handleChangeRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value });
  }

  handleRequestSort (columnName) {
    const orderBy = columnName;
    let newDirection = 'desc';

    if (this.props.orderBy === columnName && this.props.direction === 'desc') {
      newDirection = 'asc';
    }
    this.props.dispatch(changeColumnSort({
      direction: newDirection,
      orderBy: columnName
    }));
    this.props.dispatch(fetchFlights({
      direction: newDirection,
      orderBy: columnName,
      selectedAircraft: this.props.selectedAircraft
    }));
  }
}
