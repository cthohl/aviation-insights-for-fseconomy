import axios from 'axios';

export function selectAircraft(payload) {
  return {
    type: 'SELECT_AIRCRAFT',
    payload: payload
  };
}

export function changeColumnSort(payload) {
  return {
    type: 'CHANGE_COLUMN_SORT',
    payload: payload
  };
}

export function fetchFlights({
  selectedAircraft: aircraft = '',
  direction = 'desc',
  orderBy: order_by = 'profit'
}) {
  return (dispatch) => {
    axios.get('/api/flights', {
      params: {
        aircraft,
        direction,
        order_by
      }
    }).then((response) => {
      return dispatch({
        type: 'FETCH_FLIGHTS_FULFILLED',
        payload: {
          flights: response.data
        }
      });
    }).catch((error) => {
      return dispatch({ type: 'FETCH_FLIGHTS_REJECTED', payload: error });
    });
  };
}

export function fetchAircraftTypes() {
  return (dispatch) => {
    axios.get('/api/aircraft_types')
      .then((response) => {
        return dispatch({
          type: 'FETCH_AIRCRAFT_TYPES_FULFILLED',
          payload: {
            aircraft_types: response.data
          }
        });
      })
      .catch((error) => {
        return dispatch({ type: 'FETCH_AIRCRAFT_TYPES_REJECTED', payload: error });
      });
  };
}

