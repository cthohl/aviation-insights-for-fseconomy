class FlightsController < ApplicationController

  # GET /flights
  def index
    order_by = params[:order_by]
    direction = params[:direction]
    aircraft_make_model = params[:aircraft]

    flights = Flight.with_rentable_aircraft(
      aircraft_make_model,
      order_by,
      direction
    )

    render json: flights
  end
end
