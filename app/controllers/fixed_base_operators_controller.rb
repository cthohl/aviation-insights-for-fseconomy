# frozen_string_literal: true

# FixedBaseOperatorsController
class FixedBaseOperatorsController < ApplicationController
  # Get the FBOs which are the best and worst "value" for the money
  def best_and_worst_value
    best_value = FixedBaseOperator
                 .where('price_to_cost_ratio > 0')
                 .order('price_to_cost_ratio ASC NULLS LAST')
                 .first

    worst_value = FixedBaseOperator
                  .where('price_to_cost_ratio > 0')
                  .order('price_to_cost_ratio DESC NULLS LAST')
                  .first

    render json: { best_value: best_value, worst_value: worst_value }
  end
end
