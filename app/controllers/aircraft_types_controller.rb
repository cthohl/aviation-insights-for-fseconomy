# frozen_string_literal: true

# AircraftTypesController
class AircraftTypesController < ApplicationController
  # GET /aircraft_types
  def index
    aircraft_types = AircraftType.order(make_model: 'ASC')

    render json: aircraft_types
  end
end
