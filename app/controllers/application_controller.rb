# frozen_string_literal: true

# ApplicationController
class ApplicationController < ActionController::API
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :null_session
  # ^^ undefined method `protect_from_forgery'

  def index
    render 'index.html.erb'
  end

  def api
    render json: 'Hello World!'
  end
end
