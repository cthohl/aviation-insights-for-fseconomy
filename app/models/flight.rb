# frozen_string_literal: true

# A class for storing groups of jobs.
class Flight
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON

  attr_accessor :from_icao, :to_icao, :distance, :airport_id, :revenue,
                :revenue_per_nautical_mile, :booking_fee, :ground_crew_fee,
                :seats_required, :cargo_and_passenger_weight_kg, :profit,
                :profit_per_nautical_mile, :aircraft_serial_number,
                :aircraft_registration, :aircraft_available_payload_kg

  def attributes
    instance_values
  end

  def self.new_from_jobs(array_of_jobs)
    new(
      from_icao: array_of_jobs.first['location'],
      to_icao: array_of_jobs.first['to_icao'],
      distance: array_of_jobs.first['distance'],
      airport_id: array_of_jobs.first['airport_id'],
      revenue: array_of_jobs.first['revenue'],
      revenue_per_nautical_mile: array_of_jobs.first['revenue_per_nautical_mile'],
      booking_fee: nil, # TODO
      ground_crew_fee: nil, # TODO
      seats_required: array_of_jobs.first['accumulated_seats_required'],
      cargo_and_passenger_weight_kg: array_of_jobs.first['accumulated_weight_kg'],
      profit: nil, # TODO
      profit_per_nautical_mile: nil, # TODO
      aircraft_serial_number: array_of_jobs.first['aircraft_serial_number'],
      aircraft_registration: array_of_jobs.first['registration'],
      aircraft_available_payload_kg: array_of_jobs.first['available_payload_kg']
    )
  end

  def self.create_from_airport(airport_object)
    airport = Airport.find(airport_object.id)
    jobs = airport.job
    flights_created_count = 0

    jobs_by_destination = jobs.group_by { |job| job[:toicao] }
    jobs_by_destination.each do |toicao, jobs|
      flight = self.new_from_jobs(jobs)
      airport.flight.create(flight.attributes)
      flights_created_count += 1
    end

    flights_created_count
  end

  def self.create_from_airports(array_of_airports)
    flights_created_count = 0

    array_of_airports.each do |airport|
      flights_created_count += self.create_from_airport(airport)
    end

    flights_created_count
  end

  def self.refresh_flights_batch(airports)
    # Delete all Flights for the current batch of airports.
    Flight.where(airport_id: airports.ids).delete_all

    Flight.create_from_airports(airports)
  end

  def self.refresh_by_highest_profit
    airports = Airport.select('airports.*, flights.profit')
      .joins(:flight)
      .order('flights.profit desc')
      .distinct
      .limit(1000)

    jobs_inserted = Job.refresh_jobs_batch(airports)
    flights_inserted = self.refresh_flights_batch(airports)
    {jobs_inserted: jobs_inserted, flights_inserted: flights_inserted}
  end

  # Make Flights look prettier in the terminal.
  def to_s
    "from: #{self.from}, to: #{self.to}, distance: #{self.distance}, profit: #{self.profit}, profit_per_nautical_mile: #{self.profit_per_nautical_mile}, passengers: #{self.passenger_count} #{self.passenger_description || 'passengers'}, cargo: #{self.cargo_weight} #{self.cargo_description || 'of cargo'}"
  end

  def calculate_booking_fee(jobs)
    passenger_terminal_assignments = jobs.select {|job| job.ptassignment === "true"}
    jobs_count = passenger_terminal_assignments.count
    booking_fee = 0

    if (jobs_count > 5)
      booking_fee = self.revenue * (jobs_count / 100.0)
    end

    booking_fee
  end

  def calculate_ground_crew_fee
    # 0%, 5%, or 10% based of presence of private FBOs at origin and destination
    number_of_fbos = self.tally_player_owned_fbos
    fee_percentage = number_of_fbos * 0.05
    return self.revenue * fee_percentage
  end

  def tally_player_owned_fbos
    return 2 unless origin_and_destination_fbos_fetched?
    fbo_count = 0
    fbo_count += self.has_player_owned_fbo(self.from_icao) ? 1 : 0
    fbo_count += self.has_player_owned_fbo(self.to_icao) ? 1 : 0
    fbo_count
  end

  def origin_and_destination_fbos_fetched?
    airports = Airport.where('icao = ? AND fbos_fetched_at IS NULL', self.from_icao).or(Airport.where('icao = ? AND fbos_fetched_at IS NULL', self.to_icao))
    return airports.count === 0
  end

  def has_player_owned_fbo(icao)
    fbos = FixedBaseOperator.where('icao = ? AND fse_id IS NOT NULL', icao)
    return fbos.count > 0
  end

  def count_passengers(jobs)
    jobs_with_passengers = jobs.select {|job| job.unittype === "passengers"}
    return jobs_with_passengers.reduce(0) { |passenger_count, job| passenger_count += job.amount.to_i }
  end

  def describe_passengers(jobs)
    jobs_with_passengers = jobs.select {|job| job.unittype === "passengers"}
    return jobs_with_passengers.first && jobs_with_passengers.first.commodity
  end

  def weigh_cargo(jobs)
    jobs_with_cargo = jobs.select {|job| job.unittype === "kg"}
    return jobs_with_cargo.reduce(0) { |cargo_weight, job| cargo_weight += job.amount.to_i }
  end

  def describe_cargo(jobs)
    jobs_with_cargo = jobs.select {|job| job.unittype === "kg"}
    return jobs_with_cargo.first && jobs_with_cargo.first.commodity
  end

  def calculate_profit_per_nm(profit, distance)

    if distance.zero?
      return profit
    else
      return profit / distance
    end
  end

  def self.with_rentable_aircraft(
    aircraft_make_model = nil,
    order_by_field = 'revenue',
    direction = 'DESC'
  )
    aircraft_make_model = 'Beechcraft 1900D' if aircraft_make_model.blank?
    # This query still needs some work. Sometimes I get back jobs where the
    # aircraft isn't available because it has been grounded (FeeOwed is > 0).
    # And I'm imagining that this query will provide the data used to create
    # Flight objects, and Flights will no longer be persisted. They'll just be
    # created from persisted Jobs and Aircraft.
    ActiveRecord::Base
      .connection
      .exec_query("
        WITH
          jobs_with_rentable_aircraft AS (
            SELECT
              j.*,
              SUM(j.weight_kg) OVER (
                PARTITION BY j.location, j.to_icao, ac1.id
                ORDER BY j.pay_per_nautical_mile DESC, j.id ASC
              ) AS accumulated_weight_kg,
              SUM(j.seats_required) OVER (
                PARTITION BY j.location, j.to_icao, ac1.id
                ORDER BY j.pay_per_nautical_mile DESC, j.id ASC
              ) AS accumulated_seats_required,
              ac1.id as aircraft_id,
              ac1.serial_number as aircraft_serial_number,
              ac1.registration,
              CAST(ac1.available_payload_kg AS DOUBLE PRECISION),
              ac1.available_seats
            FROM   jobs j
            JOIN   aircrafts ac1 ON j.airport_id = ac1.airport_id
            WHERE  ac1.makemodel = '#{aircraft_make_model}'
            AND    ac1.fee_owed = 0 -- NOT grounded due to fees owed
            AND   (ac1.rental_dry > 0 OR ac1.rental_wet > 0) -- IS rentable
          ),
          flyable_jobs AS (
            SELECT
              *,
              SUM(jwra.pay) OVER (
                PARTITION BY jwra.location, jwra.to_icao, jwra.aircraft_id
              ) AS revenue,
              SUM(jwra.pay_per_nautical_mile) OVER (
                PARTITION BY jwra.location, jwra.to_icao, jwra.aircraft_id
              ) AS revenue_per_nautical_mile,
              RANK() OVER (
                ORDER BY jwra.location, jwra.to_icao, jwra.aircraft_id
              ) AS flight_group
            FROM jobs_with_rentable_aircraft jwra
            WHERE jwra.accumulated_weight_kg <= jwra.available_payload_kg
            AND jwra.accumulated_seats_required <= jwra.available_seats
          )
        SELECT *
        FROM flyable_jobs
        ORDER BY #{order_by_field} #{direction}, available_payload_kg DESC
      ")
      .to_hash
      .group_by { |job| job['flight_group'] }
      .map { |rank, jobs| Flight.new_from_jobs(jobs) }
  end
end
