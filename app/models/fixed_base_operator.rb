# fixed_base_operator.rb

class FixedBaseOperator < ApplicationRecord
  belongs_to :airport

  def self.new_from_xml(fbo_as_xml)
    new_fbo = self.new do |fbo|

      if fbo_as_xml.at_xpath('xmlns:FboId')
        fbo.fse_id = fbo_as_xml.at_xpath('xmlns:FboId').content
      else
        fbo.fse_id = fbo_as_xml.at_xpath('xmlns:FboId')
      end
      fbo.status = fbo_as_xml.at_xpath('xmlns:Status').content
      fbo.airport_name = fbo_as_xml.at_xpath('xmlns:Airport').content
      fbo.name = fbo_as_xml.at_xpath('xmlns:Name').content
      fbo.owner = fbo_as_xml.at_xpath('xmlns:Owner').content
      fbo.icao = fbo_as_xml.at_xpath('xmlns:Icao').content
      fbo.location = fbo_as_xml.at_xpath('xmlns:Location').content
      fbo.lots = fbo_as_xml.at_xpath('xmlns:Lots').content
      fbo.repair_shop = fbo_as_xml.at_xpath('xmlns:RepairShop').content
      fbo.gates = fbo_as_xml.at_xpath('xmlns:Gates').content
      fbo.gates_rented = fbo_as_xml.at_xpath('xmlns:GatesRented').content
      fbo.fuel_100ll = fbo_as_xml.at_xpath('xmlns:Fuel100LL').content
      fbo.fuel_jeta = fbo_as_xml.at_xpath('xmlns:FuelJetA').content
      fbo.building_materials = fbo_as_xml.at_xpath('xmlns:BuildingMaterials').content
      fbo.supplies = fbo_as_xml.at_xpath('xmlns:Supplies').content
      fbo.supplies_per_day = fbo_as_xml.at_xpath('xmlns:SuppliesPerDay').content
      fbo.supplied_days = fbo_as_xml.at_xpath('xmlns:SuppliedDays').content
      fbo.sell_price = fbo_as_xml.at_xpath('xmlns:SellPrice').content
    end

    if (new_fbo.fse_id) # Only player-owned FBOs have an fse_id
      new_fbo.estimated_cost = new_fbo.estimate_cost_to_build_and_supply
      new_fbo.price_to_cost_ratio = new_fbo.sell_price / new_fbo.estimated_cost
    end

    return new_fbo
  end

  def self.download_and_save_for_sale
    fbos_saved = 0
    response_xml = FseApi.make_request({
        query: 'fbos',
       search: 'forsale'
    })
    fbos_saved += create_for_sale_from_xml(response_xml)
  end

  def self.download_and_save_by_airport(airport)
    fbos_saved = 0
    response_xml = FseApi.make_request({
        query: 'icao',
       search: 'fbo',
         icao: airport.icao
    })
    fbos_saved += create_from_xml(response_xml, airport)
  end

  def self.create_for_sale_from_xml(response_xml)

    # Check for <Error> element in XML response.
    xml_error_message = Nokogiri::XML(response_xml).xpath("/Error")
    unless (xml_error_message.empty?)
      raise RuntimeError, "Found error message in XML response: #{xml_error_message.text}"
    end

    xml_fbos = Nokogiri::XML(response_xml).xpath(
      "/fs:FboItems/fs:FBO",
      "fs" => "http://server.fseconomy.net"
    )

    fbo_objects = xml_fbos.map do |fbo|
      self.new_from_xml(fbo)
    end

    Airport.transaction do
      fbos_created_count = 0
      fbo_objects.each do |fbo|
        old_fbo_record = FixedBaseOperator.where(fse_id: fbo.fse_id).first
        old_fbo_record.destroy unless !old_fbo_record
        airport = Airport.where(icao: fbo.icao).first
        airport.fixed_base_operators.create(fbo.attributes)
        fbos_created_count += 1
      end
      fbos_created_count
    end
  end

  def self.create_from_xml(response_xml, airport_object)

    # Check for <Error> element in XML response.
    xml_error_message = Nokogiri::XML(response_xml).xpath("/Error")
    unless (xml_error_message.empty?)
      raise RuntimeError, "Found error message in XML response: #{xml_error_message.text}"
    end

    xml_fbos = Nokogiri::XML(response_xml).xpath(
      "/fs:IcaoFboItems/fs:FBO",
      "fs" => "http://server.fseconomy.net"
    )

    fbo_objects = xml_fbos.map do |fbo|
      self.new_from_xml(fbo)
    end

    Airport.transaction do
      fbos_created_count = 0
      fbo_objects.each do |fbo|
        airport_object.fixed_base_operators.create(fbo.attributes)
        fbos_created_count += 1
      end
      airport_object.fbos_fetched_at = Time.now
      airport_object.save
      fbos_created_count
    end
  end

  def self.refresh_oldest_airport
    airport = Airport.order('fbos_fetched_at ASC NULLS FIRST').first
    airport.fixed_base_operators.destroy
    print "Refreshing #{airport.icao} "
    print "(fbos_fetched_at: #{airport.fbos_fetched_at})" unless !airport.fbos_fetched_at
    print "..."
    fbo_count = self.download_and_save_by_airport(airport)
    grammatical_fbo = fbo_count === 1 ? 'FBO' : 'FBOs'
    puts " #{fbo_count} #{grammatical_fbo} fetched"
  end

  def estimate_cost_to_build_and_supply()
    price_of_100ll = 1.50
    price_of_jeta = 1.80
    price_of_supplies = 6.75
    price_of_building_materials = 4.00

    cost_of_100ll = self.fuel_100ll * price_of_100ll
    cost_of_jeta = self.fuel_jeta * price_of_jeta
    cost_of_supplies = self.supplies * price_of_supplies
    cost_of_building_materials = self.building_materials * price_of_building_materials
    cost_of_inventory = cost_of_100ll + cost_of_jeta + cost_of_supplies + cost_of_building_materials

    cost_of_main_building = self.lots * 10000 * price_of_building_materials
    cost_of_repair_shop = self.repair_shop ? 2000 * price_of_building_materials : 0
    cost_of_passenger_terminal = self.gates > 0 ? 2000 * price_of_building_materials : 0
    cost_to_build_fbo = cost_of_main_building + cost_of_repair_shop + cost_of_passenger_terminal

    return cost_of_inventory + cost_to_build_fbo
  end
end

