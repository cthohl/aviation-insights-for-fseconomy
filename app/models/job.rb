# frozen_string_literal: true

require 'geocoder'

# A class for storing a single job.
class Job < ApplicationRecord
  belongs_to :airport
  has_many :aircraft, through: :airport
  # has_and_belongs_to_many :flight
  
  PASSENGER_WEIGHT_KG = 77

  # Returns the distance (in nm) between two airports.
  def self.calculate_flight_distance(icao_code1, icao_code2)
    distance_in_km = Geocoder::Calculations.distance_between(
      Airport.get_coordinates(icao_code1),
      Airport.get_coordinates(icao_code2),
      units: :km
    )
    (distance_in_km * Geocoder::Calculations::KM_IN_NM).round
  end

  # Calculates the pay per nm.
  def self.calculate_pay_per_nm(pay, distance)
    return pay if distance.zero?

    pay / distance
  end

  # Given one or more airports, download a list of jobs
  def self.download_jobs(airports)
    icaos = airports.map(&:icao)
    response_xml = FseApi.make_request(
      query: 'icao',
      search: 'jobsfrom',
      icaos: icaos.join('-')
    )

    # Check for <Error> element in XML response.
    xml_error_message = Nokogiri::XML(response_xml).xpath('/Error')
    unless xml_error_message.empty?
      raise "Found error message in XML response: #{xml_error_message.text}"
    end

    response_xml
  end

  # Given an XML response, extract and save jobs
  def self.update_or_insert_jobs_from_xml(response_xml, airports)
    xml_assignments = Nokogiri::XML(response_xml).xpath(
      '/fs:IcaoJobsFrom/fs:Assignment',
      'fs' => 'http://server.fseconomy.net'
    )
    job_objects = xml_assignments.map do |assignment|
      toicao = assignment.at_xpath('xmlns:ToIcao').content
      fromicao = assignment.at_xpath('xmlns:FromIcao').content
      location = assignment.at_xpath('xmlns:Location').content
      current_airport = airports.find { |airport| airport.icao == location }
      distance = calculate_flight_distance(location, toicao)

      # Convert :pay from float to Money.
      pay = Money.new(assignment.at_xpath('xmlns:Pay').content, 'USD') * 100
      is_pt_assignment = assignment.at_xpath('xmlns:PtAssignment').content.downcase == 'true'
      unit_type = assignment.at_xpath('xmlns:UnitType').content
      amount = assignment.at_xpath('xmlns:Amount').content

      ppnm = calculate_pay_per_nm(pay, distance)

      Job.new(
        airport_id: current_airport.id,
        amount: amount,
        booking_fee: calculate_booking_fee(is_pt_assignment, pay),
        commodity: assignment.at_xpath('xmlns:Commodity').content,
        distance: distance,
        expires_at: assignment.at_xpath('xmlns:ExpireDateTime').content,
        express: assignment.at_xpath('xmlns:Express').content,
        from_icao: fromicao,
        fse_id: assignment.at_xpath('xmlns:Id').content,
        job_type: assignment.at_xpath('xmlns:Type').content,
        location: location,
        passenger_terminal_assignment: is_pt_assignment,
        pay: pay,
        pay_per_nautical_mile: ppnm,
        seats_required: calculate_seats_required(unit_type, amount),
        to_icao: toicao,
        unit_type: unit_type,
        weight_kg: calculate_weight_kg(unit_type, amount)
      )
    end

    jobs_by_location = job_objects.group_by { |job| job[:location] }
    airport_ids_string = airports.map { |airport| "#{airport.id}" }.sort.join(', ')

    mark_airports_jobs_as_fetched(airport_ids_string)

    ActiveRecord::Base.transaction do
      delete_inactive_jobs(jobs_by_location, airport_ids_string)
      Job.import(
        job_objects,
        on_duplicate_key_update: {
          conflict_target: [:fse_id],
          columns: [
            :airport_id,
            :amount,
            :booking_fee,
            :commodity,
            :distance,
            :expires_at,
            :express,
            :from_icao,
            :job_type,
            :location,
            :passenger_terminal_assignment,
            :pay,
            :pay_per_nautical_mile,
            :seats_required,
            :to_icao,
            :unit_type,
            :weight_kg
          ]
        }
      )
      job_objects.count
    end
  end

  def self.mark_airports_jobs_as_fetched(airport_ids_string)
    ActiveRecord::Base
      .connection
      .exec_query("
        UPDATE airports
        SET
          updated_at = LOCALTIMESTAMP,
          jobs_last_fetched = LOCALTIMESTAMP
        WHERE airports.id IN (#{airport_ids_string})
      ")
  end

  def self.calculate_weight_kg(unit_type, amount)
    return amount.to_i if unit_type.casecmp('kg').zero?

    amount.to_i * PASSENGER_WEIGHT_KG
  end

  def self.calculate_seats_required(unit_type, amount)
    return 0 if unit_type.casecmp('kg').zero?

    amount.to_i
  end

  def self.calculate_booking_fee(is_pt_assignment, pay)
    return 0 unless is_pt_assignment

    pay * 0.01 # one percent
  end

  def self.refresh_jobs_batch(airport_ids)
    beginning_time = Time.now
    initial_job_count = Job.count

    airports = Airport.find(airport_ids)

    job_xml = Job.download_jobs(airports)
    jobs_inserted = 0
    jobs_inserted += update_or_insert_jobs_from_xml(job_xml, airports)

    end_time = Time.now
    elapsed_time = end_time - beginning_time

    total_jobs = Job.count
    new_jobs = sprintf("%+d", total_jobs - initial_job_count)
    jobs_per_second = (jobs_inserted / elapsed_time).round(2)

    puts "#{jobs_inserted} jobs saved in #{elapsed_time.round(2)} seconds at"\
      " #{jobs_per_second} jobs per second (#{new_jobs} total jobs)"
  end

  def self.delete_inactive_jobs(jobs_by_location_icao, airport_ids_string)
    existing_job_fse_ids = ActiveRecord::Base
                           .connection
                           .exec_query("
                             SELECT fse_id
                             FROM jobs
                             WHERE airport_id IN (#{airport_ids_string})
                             ORDER BY fse_id ASC
                           ")
                           .rows
                           .flatten

    new_job_fse_ids = jobs_by_location_icao.values.flatten.map { |job| job[:fse_id] }
    inactive_job_fse_ids = existing_job_fse_ids - new_job_fse_ids

    ActiveRecord::Base
      .connection
      .exec_query("
        DELETE FROM jobs
        WHERE jobs.fse_id IN (#{inactive_job_fse_ids.sort.join(', ')})
      ") unless inactive_job_fse_ids.empty?

    jobs_by_location_icao
  end
end
