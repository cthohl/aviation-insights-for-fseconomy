# fse_api.rb
#
# A class for communicating with the FSEconomy API.

require 'dotenv/load'
require 'net/http'

class FseApi
  def make_request(new_params)
    uri = URI(ENV['FSECONOMY_ENDPOINT_BASE'])
    default_params = {
      servicekey: ENV['FSECONOMY_SERVICE_KEY'],
       format: 'xml'
    }
    params = default_params.merge(new_params)
    uri.query = URI.encode_www_form(params)

    response = Net::HTTP.get_response(uri)
    error_message = "Received #{response.code} #{response.message} from fseconomy."
    raise RuntimeError, error_message unless response.is_a?(Net::HTTPSuccess)

    response.body
  end

  def self.make_request(new_params)
    self.new.make_request(new_params)
  end

  def self.make_request_for_airport_zip_file
    uri = URI(ENV['FSECONOMY_ENDPOINT_AIRPORTS'])
    Net::HTTP.start(uri.host, uri.port) do |http|
      request = Net::HTTP::Get.new uri
      http.request request do |response|
        yield(response)
      end
    end
  end
end
