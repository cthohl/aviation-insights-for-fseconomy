require 'csv'
require 'dotenv/load'
require 'zip'

class Airport < ApplicationRecord
    has_many :aircraft, dependent: :destroy
    has_many :job, dependent: :destroy
    has_many :flight, dependent: :destroy
    has_many :fixed_base_operators, dependent: :destroy

    def self.request_airports(zip_file_path = './data/datafeed_icaodata.zip')
        puts 'Downloading airport data from fseconomy...'
        FseApi.make_request_for_airport_zip_file do |net_http_response|
          puts "Creating #{zip_file_path}..."
          File.open(zip_file_path, 'wb') do |file|
            net_http_response.read_body do |chunk|
              file.write(chunk)
            end
          end
        end
        self.extract_airports_zip(zip_file_path)
        puts "Deleting #{zip_file_path}..."
        File.delete(zip_file_path)
    end

    def self.extract_airports_zip(zip_file_path)
        puts "Extracting #{zip_file_path}..."
        csv_file_path = nil
        Zip::File.open(zip_file_path) do |zipfile|
            zipfile.each do |file|
                csv_file_path = "./data/#{file.name}"
                puts "Creating #{csv_file_path}..."
                file.extract(csv_file_path)
            end
        end
        create_airports_from_csv(csv_file_path)
        puts "Deleting #{csv_file_path}..."
        File.delete(csv_file_path)
    end

    def self.create_airports_from_csv(csv_file_path)
        puts "Seeding Airports table with #{csv_file_path}..."
        csv_text = File.read(csv_file_path)
        csv = CSV.parse(
            csv_text,
            headers: true,
            header_converters: lambda do |header|
                return 'designation' if header === 'type'
                return 'runway_length' if header === 'size'
                return header
            end
        )
        csv.each do |row|
            row_as_hash = row.to_hash
            self.create(row_as_hash)
        end
    end

    # Returns an array representation of [lat, lng].
    def self.get_coordinates(icao_code)

        # Find the icao_code in the table.
        airport = self.find_by(icao: icao_code)

        if airport.nil? # We're about to throw an error
            puts "Unable to find #{icao_code}. Does the table have data?"
        end

        return [airport.lat, airport.lon]
    end

    def request_jobs()
        Job.request_jobs(self.icao)
    end
end

