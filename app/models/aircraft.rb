# Aircraft.rb
#
# A class for storing a single aircraft.

class Aircraft < ApplicationRecord
  belongs_to :airport
  has_many :job, through: :airport
  has_many :flight, through: :airport

  # Taken from https://sites.google.com/site/fseoperationsguide/faq?q=6
  GALLONS_TO_KILOGRAMS_CONVERSION = 2.687344961

  SINGLE_PILOT_MTOW_LIMIT = 5670

  # Given a make and model, request a list of aircraft
  def self.request_aircraft(make_model = nil)
    make_model ||= AircraftType.order(aircraft_fetched_at: 'desc').first.make_model

    aircraft_type = AircraftType.find_by(make_model: make_model)
    raise "No such aircraft type: #{make_model}" if aircraft_type.nil?

    initial_aircraft_count = Aircraft.count

    response_xml = FseApi.make_request(
      query: 'aircraft',
      search: 'makemodel',
      makemodel: make_model
    )

    # Check for <Error> element in XML response.
    xml_error_message = Nokogiri::XML(response_xml).xpath('/Error')
    unless xml_error_message.empty?
      raise "Found error message in XML response: #{xml_error_message.text}"
    end

    create_aircraft_from_xml(aircraft_type, response_xml)

    mark_aircraft_as_fetched(aircraft_type.id)

    new_count = Aircraft.count
    difference = new_count - initial_aircraft_count
    puts "Aircraft downloaded: #{difference == 0 ? 'same count' : difference}"
  end

  def self.create_aircraft_from_xml(aircraft_type, response_xml)
    xml_aircrafts = Nokogiri::XML(response_xml).xpath(
      '/fs:AircraftItems/fs:Aircraft',
      'fs' => 'http://server.fseconomy.net'
    )

    xml_aircrafts.each do |xml_aircraft|
      aircraft_location = xml_aircraft.at_xpath('xmlns:Location').content
      next if aircraft_location == 'In Flight'

      airport = Airport.find_by(icao: aircraft_location)
      current_fuel_percentage = xml_aircraft.at_xpath('xmlns:FuelPct').content
      current_fuel_gallons = calculate_current_fuel_gallons(aircraft_type, current_fuel_percentage)
      current_fuel_kg = calculate_current_fuel_kg(current_fuel_gallons)
      available_payload_kg = calculate_available_payload_kg(aircraft_type, current_fuel_kg)

      Aircraft
        .find_or_initialize_by(
          serial_number: xml_aircraft.at_xpath('xmlns:SerialNumber').content
        )
        .update(
          registration: xml_aircraft.at_xpath('xmlns:Registration').content,
          makemodel: xml_aircraft.at_xpath('xmlns:MakeModel').content,
          needsrepair: xml_aircraft.at_xpath('xmlns:NeedsRepair').content == '1',
          owner: xml_aircraft.at_xpath('xmlns:Owner').content,
          rental_dry: xml_aircraft.at_xpath('xmlns:RentalDry').content,
          rental_wet: xml_aircraft.at_xpath('xmlns:RentalWet').content,
          rental_type: xml_aircraft.at_xpath('xmlns:RentalType').content,
          rental_time: xml_aircraft.at_xpath('xmlns:RentalTime').content,
          rented_by: xml_aircraft.at_xpath('xmlns:RentedBy').content,
          location: aircraft_location,
          airport_id: airport.id,
          current_fuel_percentage: current_fuel_percentage,
          current_fuel_gallons: current_fuel_gallons,
          current_fuel_kg: current_fuel_kg,
          available_payload_kg: available_payload_kg,
          available_seats: calculate_available_seats(aircraft_type),
          fee_owed: Money.new(xml_aircraft.at_xpath('xmlns:FeeOwed').content, 'USD') * 100
        )
    end
  end

  def self.calculate_current_fuel_gallons(aircraft_type, current_fuel_percentage)
    aircraft_type.fuel_capacity_gallons * current_fuel_percentage.to_f
  end

  def self.calculate_current_fuel_kg(current_fuel_gallons)
    current_fuel_gallons * GALLONS_TO_KILOGRAMS_CONVERSION
  end

  def self.calculate_available_payload_kg(aircraft_type, current_fuel_kg)
    aircraft_type.useful_load_kg - aircraft_type.crew_weight_kg - current_fuel_kg
  end

  def self.calculate_available_seats(aircraft_type)
    number_of_pilots = aircraft_type.maximum_takeoff_weight > SINGLE_PILOT_MTOW_LIMIT ? 2 : 1

    aircraft_type.seats - number_of_pilots
  end

  def self.mark_aircraft_as_fetched(aircraft_type_id)
    ActiveRecord::Base
      .connection
      .exec_query("
        UPDATE aircraft_types
        SET
          updated_at = LOCALTIMESTAMP,
          aircraft_fetched_at = LOCALTIMESTAMP
        WHERE aircraft_types.id = #{aircraft_type_id}
      ")
  end
end
