# frozen_string_literal: true

# For behavior that's common to all aircraft of a type
class AircraftType < ApplicationRecord
  PILOT_SEAT = 1
  PASSENGER_WEIGHT_KG = 77

  def self.refresh(fse_api = FseApi.new)
    response_xml = fse_api.make_request({
       query: 'aircraft',
      search: 'configs'
    })

    update_or_insert_aircraft_types_from_xml(response_xml)
  end

  def self.update_or_insert_aircraft_types_from_xml(response_xml)

    xml_assignments = Nokogiri::XML(response_xml).xpath(
      "/fs:AircraftConfigItems/fs:AircraftConfig",
      "fs" => "http://server.fseconomy.net"
    )

    xml_assignments.each do |aircraft_config|
      crew = aircraft_config.at_xpath('xmlns:Crew').content
      seats = aircraft_config.at_xpath('xmlns:Seats').content
      mtow = aircraft_config.at_xpath('xmlns:MTOW').content
      empty_weight = aircraft_config.at_xpath('xmlns:EmptyWeight').content
      AircraftType
        .find_or_initialize_by(fse_id: aircraft_config.at_xpath('xmlns:ModelId').content)
        .update({
          make_model: aircraft_config.at_xpath('xmlns:MakeModel').content,
          crew: crew,
          seats: seats,
          cruise_speed: aircraft_config.at_xpath('xmlns:CruiseSpeed').content,
          gallons_per_hour: aircraft_config.at_xpath('xmlns:GPH').content,
          fuel_type: aircraft_config.at_xpath('xmlns:FuelType').content,
          maximum_takeoff_weight: mtow,
          empty_weight: empty_weight,
          engine_count: aircraft_config.at_xpath('xmlns:Engines').content,
          useful_load_kg: calculate_useful_load_kg(mtow, empty_weight),
          crew_weight_kg: calculate_crew_weight_kg(crew),
          fuel_capacity_gallons: calculate_fuel_capacity_gallons(
            aircraft_config.at_xpath('xmlns:Ext1').content,
            aircraft_config.at_xpath('xmlns:LTip').content,
            aircraft_config.at_xpath('xmlns:LAux').content,
            aircraft_config.at_xpath('xmlns:LMain').content,
            aircraft_config.at_xpath('xmlns:Center1').content,
            aircraft_config.at_xpath('xmlns:Center2').content,
            aircraft_config.at_xpath('xmlns:Center3').content,
            aircraft_config.at_xpath('xmlns:RMain').content,
            aircraft_config.at_xpath('xmlns:RAux').content,
            aircraft_config.at_xpath('xmlns:RTip').content,
            aircraft_config.at_xpath('xmlns:Ext2').content
          )
        })
    end
  end

  def self.calculate_useful_load_kg(mtow, empty_weight)
    mtow.to_i - empty_weight.to_i
  end

  def self.calculate_crew_weight_kg(crew_seats)
    (crew_seats.to_i + PILOT_SEAT.to_i) * PASSENGER_WEIGHT_KG
  end

  def self.calculate_fuel_capacity_gallons(
    ext1,
    l_tip,
    l_aux,
    l_main,
    center1,
    center2,
    center3,
    r_main,
    r_aux,
    r_tip,
    ext2
  )
    ext1.to_i +
      l_tip.to_i +
      l_aux.to_i +
      l_main.to_i +
      center1.to_i +
      center2.to_i +
      center3.to_i +
      r_main.to_i +
      r_aux.to_i +
      r_tip.to_i +
      ext2.to_i
  end
end

