class CreateAirports < ActiveRecord::Migration[4.2]
  def change
    create_table :airports do |t|
      t.string :icao
      t.string :lat
      t.string :lon
      t.string :designation
      t.integer :runway_length
      t.string :name
      t.string :city
      t.string :state
      t.string :country
      t.timestamp :jobs_last_fetched

      t.timestamps null: false
    end
  end
end
