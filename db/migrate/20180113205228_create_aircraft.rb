class CreateAircraft < ActiveRecord::Migration[4.2]
  def change
    create_table :aircrafts do |t|
      t.string :makemodel
      t.string :owner
      t.string :location
      t.boolean :needsrepair

      t.timestamps null: false
    end
  end
end
