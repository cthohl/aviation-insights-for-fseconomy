class ChangePassengerTerminalAssignmentColumnToBoolean < ActiveRecord::Migration[5.1]
  def change
    change_column :jobs, :passenger_terminal_assignment, 'boolean USING CAST(passenger_terminal_assignment AS boolean)'
  end
end
