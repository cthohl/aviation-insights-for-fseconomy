class AddAircraftBelongToAirportRelationship < ActiveRecord::Migration[4.2]
    def change
        change_table :aircrafts do |t|
            t.belongs_to :airport
        end
    end
end
