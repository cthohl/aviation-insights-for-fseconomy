class AddJobBelongToAirportRelationship < ActiveRecord::Migration[4.2]
  def change
        change_table :jobs do |t|
            t.belongs_to :airport
        end
  end
end
