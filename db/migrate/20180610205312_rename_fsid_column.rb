class RenameFsidColumn < ActiveRecord::Migration[5.1]
  def change
    rename_column :jobs, :fs_id, :fse_id
    add_index :jobs, :fse_id
  end
end
