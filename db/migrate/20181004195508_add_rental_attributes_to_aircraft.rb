class AddRentalAttributesToAircraft < ActiveRecord::Migration[5.1]
  def change
    add_column :aircrafts, :rental_dry, :integer
    add_column :aircrafts, :rental_wet, :integer
    add_column :aircrafts, :rental_type, :string
    add_column :aircrafts, :rental_time, :integer
    add_column :aircrafts, :rented_by, :string
  end
end
