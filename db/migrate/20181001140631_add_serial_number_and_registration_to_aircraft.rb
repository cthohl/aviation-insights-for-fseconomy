class AddSerialNumberAndRegistrationToAircraft < ActiveRecord::Migration[5.1]
  def change
    add_column :aircrafts, :serial_number, :integer
    add_index :aircrafts, :serial_number
    add_column :aircrafts, :registration, :string
  end
end
