class CreateJobs < ActiveRecord::Migration[4.2]
    def change
        create_table :jobs do |t|

            t.integer :fs_id
            t.string :location
            t.string :toicao
            t.string :fromicao
            t.integer :amount
            t.string :unittype
            t.string :commodity
            t.integer :pay
            t.datetime :expiresdatetime
            t.string :job_type
            t.string :express
            t.string :ptassignment
            t.decimal :distance
            t.integer :ppnm

            t.timestamps null: false
        end
    end
end
