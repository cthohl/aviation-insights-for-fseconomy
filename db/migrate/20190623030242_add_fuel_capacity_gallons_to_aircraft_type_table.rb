class AddFuelCapacityGallonsToAircraftTypeTable < ActiveRecord::Migration[5.1]
  def change
    add_column :aircraft_types, :fuel_capacity_gallons, :integer
  end
end
