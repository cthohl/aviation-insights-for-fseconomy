class AddTimestampsToFlight < ActiveRecord::Migration[5.1]
  def change
    add_column :flights, :created_at, :datetime, null: false
    add_column :flights, :updated_at, :datetime, null: false
  end
end
