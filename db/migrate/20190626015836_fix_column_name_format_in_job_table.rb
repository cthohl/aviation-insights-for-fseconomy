class FixColumnNameFormatInJobTable < ActiveRecord::Migration[5.1]
  def change
    rename_column :jobs, :toicao, :to_icao
    rename_column :jobs, :fromicao, :from_icao
    rename_column :jobs, :unittype, :unit_type
    rename_column :jobs, :expiresdatetime, :expires_at
    rename_column :jobs, :ptassignment, :passenger_terminal_assignment
    rename_column :jobs, :ppnm, :pay_per_nautical_mile
  end
end
