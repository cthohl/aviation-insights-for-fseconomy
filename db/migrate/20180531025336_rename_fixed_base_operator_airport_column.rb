class RenameFixedBaseOperatorAirportColumn < ActiveRecord::Migration[5.1]
  def change
    rename_column :fixed_base_operators, :airport, :airport_name
  end
end
