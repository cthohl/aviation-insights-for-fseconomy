class AddIndexToAirportIcaos < ActiveRecord::Migration[4.2]
  def change
      add_index :airports, :icao
  end
end
