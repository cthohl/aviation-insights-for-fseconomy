class AddBookingFeeToJobTable < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs, :booking_fee, :integer
  end
end
