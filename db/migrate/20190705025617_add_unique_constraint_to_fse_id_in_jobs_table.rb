class AddUniqueConstraintToFseIdInJobsTable < ActiveRecord::Migration[5.1]
  def change
    remove_index :jobs, :fse_id
    add_index :jobs, :fse_id, unique: true
  end
end
