class AddWeightKgToJobTable < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs, :weight_kg, :integer
  end
end
