class AddIndexToJobAirportIdColumn < ActiveRecord::Migration[5.1]
  def change
    add_index :jobs, :airport_id
  end
end
