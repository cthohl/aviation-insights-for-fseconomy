class AddAvailablePayloadKgToAircraftTable < ActiveRecord::Migration[5.1]
  def change
    add_column :aircrafts, :available_payload_kg, :decimal, :precision => 9, :scale => 2
  end
end
