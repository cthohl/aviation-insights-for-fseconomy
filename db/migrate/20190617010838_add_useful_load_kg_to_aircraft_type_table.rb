class AddUsefulLoadKgToAircraftTypeTable < ActiveRecord::Migration[5.1]
  def change
    add_column :aircraft_types, :useful_load_kg, :integer
  end
end
