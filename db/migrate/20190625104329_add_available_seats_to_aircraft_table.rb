class AddAvailableSeatsToAircraftTable < ActiveRecord::Migration[5.1]
  def change
    add_column :aircrafts, :available_seats, :integer
  end
end
