class AddSeatsRequiredToJobTable < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs, :seats_required, :integer
  end
end
