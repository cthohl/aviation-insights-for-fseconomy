class CreateAircraftTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :aircraft_types do |t|
      t.integer :fse_id
      t.string :make_model
      t.integer :crew
      t.integer :seats
      t.integer :cruise_speed
      t.integer :gallons_per_hour
      t.integer :fuel_type
      t.integer :maximum_takeoff_weight
      t.integer :empty_weight
      t.integer :engine_count
      t.timestamps
    end
  end
end
