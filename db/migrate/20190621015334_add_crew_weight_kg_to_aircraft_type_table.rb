class AddCrewWeightKgToAircraftTypeTable < ActiveRecord::Migration[5.1]
  def change
    add_column :aircraft_types, :crew_weight_kg, :integer
  end
end
