class AddPriceToCostRatioColumnToFixedBaseOperatorsTable < ActiveRecord::Migration[5.1]
  def change
    add_column :fixed_base_operators, :price_to_cost_ratio, :decimal
  end
end
