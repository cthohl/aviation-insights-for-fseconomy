class AddFeeOwedToAircraftTable < ActiveRecord::Migration[5.1]
  def change
    add_column :aircrafts, :fee_owed, :integer
  end
end
