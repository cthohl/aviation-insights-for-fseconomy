class AddCurrentFuelGallonsToAircraftTable < ActiveRecord::Migration[5.1]
  def change
    add_column :aircrafts, :current_fuel_gallons, :decimal, :precision => 9, :scale => 2
  end
end
