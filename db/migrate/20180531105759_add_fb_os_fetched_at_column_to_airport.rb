class AddFbOsFetchedAtColumnToAirport < ActiveRecord::Migration[5.1]
  def change
    add_column :airports, :fbos_fetched_at, :datetime, null: true
  end
end
