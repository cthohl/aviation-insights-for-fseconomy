class CreateJobFlightTable < ActiveRecord::Migration[5.1]
  def change
    create_table :jobs_flights, id: false do |t|
      t.belongs_to :job, index: true
      t.belongs_to :flight, index: true
    end
  end
end
