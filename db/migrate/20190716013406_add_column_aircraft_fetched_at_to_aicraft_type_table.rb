class AddColumnAircraftFetchedAtToAicraftTypeTable < ActiveRecord::Migration[5.1]
  def change
    add_column :aircraft_types, :aircraft_fetched_at, :datetime, null: true
  end
end
