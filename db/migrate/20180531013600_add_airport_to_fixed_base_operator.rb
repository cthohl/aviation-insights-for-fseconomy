class AddAirportToFixedBaseOperator < ActiveRecord::Migration[5.1]
  def change
    add_reference :fixed_base_operators, :airport, foreign_key: true
  end
end
