class CreateFixedBaseOperators < ActiveRecord::Migration[5.1]
  def change
    create_table :fixed_base_operators do |t|
      t.integer :fse_id, null: true
      t.string :status
      t.string :airport
      t.string :name
      t.string :owner
      t.string :icao
      t.string :location
      t.integer :lots
      t.boolean :repair_shop
      t.integer :gates
      t.integer :gates_rented
      t.integer :fuel_100ll
      t.integer :fuel_jeta
      t.integer :building_materials
      t.integer :supplies
      t.integer :supplies_per_day
      t.integer :supplied_days
      t.float :sell_price

      t.timestamps
    end
  end
end
