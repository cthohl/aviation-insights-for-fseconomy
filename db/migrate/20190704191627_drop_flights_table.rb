class DropFlightsTable < ActiveRecord::Migration[5.1]
  def change
    drop_table :flights
    drop_table :jobs_flights
  end
end
