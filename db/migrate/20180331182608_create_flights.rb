class CreateFlights < ActiveRecord::Migration[5.1]
  def change
    create_table :flights do |t|
      t.decimal :distance
      t.string :to_icao
      t.string :from_icao
      t.integer :revenue
      t.integer :rpnm
      t.integer :ground_crew_fee
      t.integer :booking_fee
      t.integer :profit
      t.integer :profit_pnm
      t.integer :passenger_count
      t.string :passenger_description
      t.integer :cargo_weight
      t.string :cargo_description
      t.belongs_to :airport
    end
  end
end
