class AddCurrentFuelPercentageToAircraftTable < ActiveRecord::Migration[5.1]
  def change
    add_column :aircrafts, :current_fuel_percentage, :decimal
  end
end
