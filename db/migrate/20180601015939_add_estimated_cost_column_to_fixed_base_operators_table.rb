class AddEstimatedCostColumnToFixedBaseOperatorsTable < ActiveRecord::Migration[5.1]
  def change
    add_column :fixed_base_operators, :estimated_cost, :float
  end
end
