class AddIndexToAirportJobsLastFetched < ActiveRecord::Migration[4.2]
  def change
      add_index :airports, :jobs_last_fetched
  end
end
