# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190716013406) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "aircraft_types", force: :cascade do |t|
    t.integer "fse_id"
    t.string "make_model"
    t.integer "crew"
    t.integer "seats"
    t.integer "cruise_speed"
    t.integer "gallons_per_hour"
    t.integer "fuel_type"
    t.integer "maximum_takeoff_weight"
    t.integer "empty_weight"
    t.integer "engine_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "useful_load_kg"
    t.integer "crew_weight_kg"
    t.integer "fuel_capacity_gallons"
    t.datetime "aircraft_fetched_at"
  end

  create_table "aircrafts", id: :serial, force: :cascade do |t|
    t.string "makemodel"
    t.string "owner"
    t.string "location"
    t.boolean "needsrepair"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "airport_id"
    t.integer "serial_number"
    t.string "registration"
    t.integer "rental_dry"
    t.integer "rental_wet"
    t.string "rental_type"
    t.integer "rental_time"
    t.string "rented_by"
    t.decimal "current_fuel_percentage"
    t.decimal "current_fuel_gallons", precision: 9, scale: 2
    t.decimal "current_fuel_kg", precision: 9, scale: 2
    t.decimal "available_payload_kg", precision: 9, scale: 2
    t.integer "available_seats"
    t.integer "fee_owed"
    t.index ["serial_number"], name: "index_aircrafts_on_serial_number"
  end

  create_table "airports", id: :serial, force: :cascade do |t|
    t.string "icao"
    t.string "lat"
    t.string "lon"
    t.string "designation"
    t.integer "runway_length"
    t.string "name"
    t.string "city"
    t.string "state"
    t.string "country"
    t.datetime "jobs_last_fetched"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "fbos_fetched_at"
    t.index ["icao"], name: "index_airports_on_icao"
    t.index ["jobs_last_fetched"], name: "index_airports_on_jobs_last_fetched"
  end

  create_table "fixed_base_operators", force: :cascade do |t|
    t.integer "fse_id"
    t.string "status"
    t.string "airport_name"
    t.string "name"
    t.string "owner"
    t.string "icao"
    t.string "location"
    t.integer "lots"
    t.boolean "repair_shop"
    t.integer "gates"
    t.integer "gates_rented"
    t.integer "fuel_100ll"
    t.integer "fuel_jeta"
    t.integer "building_materials"
    t.integer "supplies"
    t.integer "supplies_per_day"
    t.integer "supplied_days"
    t.float "sell_price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "airport_id"
    t.float "estimated_cost"
    t.decimal "price_to_cost_ratio"
    t.index ["airport_id"], name: "index_fixed_base_operators_on_airport_id"
  end

  create_table "jobs", id: :serial, force: :cascade do |t|
    t.integer "fse_id"
    t.string "location"
    t.string "to_icao"
    t.string "from_icao"
    t.integer "amount"
    t.string "unit_type"
    t.string "commodity"
    t.integer "pay"
    t.datetime "expires_at"
    t.string "job_type"
    t.string "express"
    t.boolean "passenger_terminal_assignment"
    t.decimal "distance"
    t.integer "pay_per_nautical_mile"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "airport_id"
    t.integer "weight_kg"
    t.integer "seats_required"
    t.integer "booking_fee"
    t.index ["airport_id"], name: "index_jobs_on_airport_id"
    t.index ["fse_id"], name: "index_jobs_on_fse_id", unique: true
  end

  add_foreign_key "fixed_base_operators", "airports"
end
