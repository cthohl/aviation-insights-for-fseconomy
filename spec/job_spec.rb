# job_spec.rb

require_relative "../FSJobFinder"

RSpec.describe Job, "#calculate_flight_distance" do
    context "given two valid icaos in Hawaii, it correctly calculates distance between" do

        # Use this unrelated job object for all tests in this group.
        raw_xml = <<-XMLNODE
            <Assignment>
            <Id>238394913</Id>
            <Location>PHJR</Location>
            <ToIcao>NPS</ToIcao>
            <FromIcao>PHJR</FromIcao>
            <Amount>3</Amount>
            <UnitType>passengers</UnitType>
            <Commodity>LanAi'r Passengers</Commodity>
            <Pay>375.00</Pay>
            <Expires>1 days</Expires>
            <ExpireDateTime>2017-03-26 06:30:26</ExpireDateTime>
            <Type>Trip-Only</Type>
            <Express>False</Express>
            <PtAssignment>false</PtAssignment>
            </Assignment>
        XMLNODE
        job = Job.new(Nokogiri::XML(raw_xml))

        {
            "PHJR" => [ # departure
                "PHNY", # arrival
                70      # distance
            ],
            "PHNL" => [
                "PHTO",
                188
            ],
            "PHLI" => [
                "PHUP",
                220
            ],
            "PHKO" => [
                "PHNL",
                142
            ],
            "PHDH" => [
                "PHJR",
                18
            ]
        }.each do | icao_from, icao_to |

            it "#{icao_from} and #{icao_to.first}" do
                result = job.calculate_flight_distance(icao_from, icao_to.first)
                expect(result).to eq icao_to.last
            end
        end
    end
end

RSpec.describe Job, "#calculate_pay_per_nm" do
    context "given a job from the @jobs_table, it correctly calculates the pay per nautical mile of flight" do

        it "and returns 53.57 for this flight from PHJR to NPS" do

            raw_xml = <<-XMLNODE
                <Assignment>
                <Id>238394913</Id>
                <Location>PHJR</Location>
                <ToIcao>NPS</ToIcao>
                <FromIcao>PHJR</FromIcao>
                <Amount>3</Amount>
                <UnitType>passengers</UnitType>
                <Commodity>LanAi'r Passengers</Commodity>
                <Pay>375.00</Pay>
                <Expires>1 days</Expires>
                <ExpireDateTime>2017-03-26 06:30:26</ExpireDateTime>
                <Type>Trip-Only</Type>
                <Express>False</Express>
                <PtAssignment>false</PtAssignment>
                </Assignment>
            XMLNODE

            job = Nokogiri::XML(raw_xml).css "Assignment"
            result = Job.new(job).ppnm
            expect(result).to eq Money.new("5357", "USD") # => $53.57
        end

        it "and returns 0.00 for this flight from 00C to NM39" do

            raw_xml = <<-XMLNODE
                <Assignment>
                <Id>227350602</Id>
                <Location>00C</Location>
                <ToIcao>NM39</ToIcao>
                <FromIcao>00C</FromIcao>
                <Amount>0</Amount>
                <UnitType>kg</UnitType>
                <Commodity>null</Commodity>
                <Pay>0.00</Pay>
                <Expires>never</Expires>
                <ExpireDateTime>9999/1/1 00:00:00</ExpireDateTime>
                <Type>Trip-Only</Type>
                <Express>False</Express>
                <PtAssignment>false</PtAssignment>
                </Assignment>
            XMLNODE

            job = Nokogiri::XML(raw_xml).css "Assignment"
            result = Job.new(job).ppnm
            expect(result).to eq Money.new("0", "USD") # => $0.00
        end
    end
end
