require_relative "../FSJobFinder"

RSpec.describe Flight do

    context "given a flight containing two jobs" do

        raw_xml = <<-XMLNODE
            <Assignment>
            <Id>238394913</Id>
            <Location>PHJR</Location>
            <ToIcao>NPS</ToIcao>
            <FromIcao>PHJR</FromIcao>
            <Amount>3</Amount>
            <UnitType>passengers</UnitType>
            <Commodity>LanAi'r Passengers</Commodity>
            <Pay>375.00</Pay>
            <Expires>1 days</Expires>
            <ExpireDateTime>2017-03-26 06:30:26</ExpireDateTime>
            <Type>Trip-Only</Type>
            <Express>False</Express>
            <PtAssignment>false</PtAssignment>
            </Assignment>
        XMLNODE
        job_one = Job.new(Nokogiri::XML(raw_xml))
        job_one.distance = 7
        job_one.ppnm = Money.new("5357", "USD") # => $53.57

        raw_xml = <<-XMLNODE
            <Assignment>
            <Id>238397428</Id>
            <Location>PHJR</Location>
            <ToIcao>NPS</ToIcao>
            <FromIcao>PHJR</FromIcao>
            <Amount>1</Amount>
            <UnitType>passengers</UnitType>
            <Commodity>LanAi'r Passengers</Commodity>
            <Pay>174.00</Pay>
            <Expires>3 days</Expires>
            <ExpireDateTime>2017-03-26 07:30:32</ExpireDateTime>
            <Type>Trip-Only</Type>
            <Express>False</Express>
            <PtAssignment>false</PtAssignment>
            </Assignment>
        XMLNODE
        job_two = Job.new(Nokogiri::XML(raw_xml))
        job_two.distance = 7
        job_two.ppnm = Money.new("2486", "USD") # => $24.86

        raw_xml = <<-XMLNODE
            <Assignment>
            <Id>227350602</Id>
            <Location>PHJR</Location>
            <ToIcao>NPS</ToIcao>
            <FromIcao>PHJR</FromIcao>
            <Amount>105</Amount>
            <UnitType>kg</UnitType>
            <Commodity>Frozen Foods</Commodity>
            <Pay>406.00</Pay>
            <Expires>never</Expires>
            <ExpireDateTime>9999/1/1 00:00:00</ExpireDateTime>
            <Type>Trip-Only</Type>
            <Express>False</Express>
            <PtAssignment>false</PtAssignment>
            </Assignment>
        XMLNODE
        job_three = Job.new(Nokogiri::XML(raw_xml))
        job_three.distance = 7
        job_three.ppnm = Money.new("0.00", "USD") # => $0.00

        jobs = [job_one, job_two, job_three]

        it "#jobs correctly returns 3" do
            flight = Flight.new(jobs)
            result = flight.jobs
            expect(result).to eq 3
        end

        it "#ppnm correctly returns 78.43" do
            flight = Flight.new(jobs)
            result = flight.ppnm
            expect(result).to eq Money.new("7843", "USD") # => $78.43
        end

        it "#distance correctly returns 7" do
            flight = Flight.new(jobs)
            result = flight.distance
            expect(result).to eq 7
        end

        it "#from correctly returns PHJR" do
            flight = Flight.new(jobs)
            result = flight.from
            expect(result).to eq "PHJR"
        end

        it "#to correctly returns NPS" do
            flight = Flight.new(jobs)
            result = flight.to
            expect(result).to eq "NPS"
        end

        it "#passengers correctly returns 4" do
            flight = Flight.new(jobs)
            result = flight.passengers
            expect(result).to eq 4
        end

        it "#pay correctly returns 955" do
            flight = Flight.new(jobs)
            result = flight.pay
            expect(result).to eq Money.new("95500", "USD") # => $955.00
        end
    end
end
