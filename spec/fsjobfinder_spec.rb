# fsjobfinder_spec.rb

require_relative "../FSJobFinder"

RSpec.describe FSJobFinder, "#create_flight_list" do
    context "given a list of jobs, groups them by common arrivals and departures and" do

        it "returns an array of Flights" do
            finder = FSJobFinder.new
            result = finder.create_flight_list(finder.jobs_table)
            expect(result[0]).to be_instance_of(Flight)
        end
    end
end

RSpec.describe FSJobFinder, "#get_coordinates" do
    context "given an icao,"  do

        {
            "0E8" => [ # departure
                "35.7177", # lat
                "-108.202" # long
            ],
            "CTS4" => [ # departure
                "45.6817", # lat
                "-72.8283" # long
            ],
        }.each do | icao, coords |

            it "returns the coordinates of the airport" do
                result = FSJobFinder::get_coordinates(icao)
                expect(result).to eq coords
            end
        end
    end
end

RSpec.describe FSJobFinder, "#get_icaos" do
    context "given a state,"  do

        it "returns an array of all airports in that state" do
            finder = FSJobFinder.new
            result = finder.get_icaos("Hawaii")
            expect(result).to be_instance_of(Array)
        end
    end
end
