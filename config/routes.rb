Rails.application.routes.draw do
  get '/', to: 'application#index'

  get 'api', to: 'application#api'

  scope '/api' do

    get 'flights', to: 'flights#index'

    get 'fixed_base_operators/best_and_worst_value'

    get 'aircraft_types', to: 'aircraft_types#index'
  end
end
