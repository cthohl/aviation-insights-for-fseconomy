namespace :fseconomy_api do
    desc "Requests airports from fseconomy and saves them to the database"
    task download_airports: :environment do
      Airport.request_airports
    end

    desc "Deletes existing airports, requests new airports from fseconomy, and saves them to the database"
    task refresh_airports: :environment do
      Airport.destroy_all
      Airport.request_airports
    end

    desc "Requests aircraft (default is Beechcraft 1900D) from fseconomy and saves them to the database"
    task :update_aircraft, [:makemodel] => :environment do |task, args|
      initial_aircraft_count = Aircraft.count
      Aircraft.request_aircraft(args[:makemodel])
      new_count = Aircraft.count
      difference = new_count - initial_aircraft_count
      puts "Aircraft downloaded: #{difference == 0 ? 'same count' : difference}"
    end

    desc "Requests aircraft types from fseconomy and saves them to the database"
    task refresh_aircraft_types: :environment do
      initial_aircraft_type_count = AircraftType.count
      AircraftType.refresh
      new_count = AircraftType.count
      difference = new_count - initial_aircraft_type_count
      puts "Aircraft types downloaded: #{difference == 0 ? 'same count' : difference}"
    end

    desc "Requests new jobs from fseconomy for a batch of airports, and replaces existing jobs in the database"
    task refresh_jobs_batch: :environment do
      raise RuntimeError, 'No airports found.' unless Airport.count > 0

      beginning_time = Time.now
      initial_job_count = Job.count

      airports = Airport.order('jobs_last_fetched ASC NULLS FIRST').limit(1000)

      jobs_inserted = Job.refresh_jobs_batch(airports)

      end_time = Time.now
      elapsed_time = end_time - beginning_time

      total_jobs = Job.count
      new_jobs = sprintf("%+d", total_jobs - initial_job_count)
      jobs_per_second = (jobs_inserted / elapsed_time).round(2)

      puts "#{jobs_inserted} jobs saved in #{elapsed_time.round(2)} seconds at"\
        " #{jobs_per_second} jobs per second (#{new_jobs} total jobs)"
    end

    desc "Creates new flights from newly-fetched jobs given a batch of airports, replacing existing flights"
    task refresh_flights_and_update_jobs_by_oldest: :environment do

      raise RuntimeError, 'No airports found.' unless Airport.count > 0

      beginning_time = Time.now

      airports = Airport.order('jobs_last_fetched ASC NULLS FIRST').limit(1000)

      jobs_inserted = Job.refresh_jobs_batch(airports)
      flights_inserted = Flight.refresh_flights_batch(airports)

      end_time = Time.now
      elapsed_time = end_time - beginning_time

      inserts_per_second = ((jobs_inserted + flights_inserted) / elapsed_time).round(2)

      puts "#{jobs_inserted} jobs and #{flights_inserted} flights saved at #{inserts_per_second} inserts per second"
    end

    desc "List the top 25 jobs in descending order by pay where an aircraft exists"
    task list_top_jobs_by_revenue: :environment do
      results = Job
        .joins(:aircraft)
        .where(aircrafts: { needsrepair: false })
        .select('"jobs"."location", toicao, distance, SUM(pay) as sum_total_pay, SUM(amount) as sum_total_amount, unittype, commodity, "aircrafts"."id" as aircraft_id')
        .group(['location', 'toicao', 'unittype', 'aircraft_id'])
        .order('sum_total_pay DESC')
        .limit(25)

      results.each do |result|
        puts "#{result.location} -> #{result.toicao} (#{result.distance}nm): $#{result.sum_total_pay || result.pay} to transport #{result.sum_total_amount || result.amount} #{result.unittype} (#{result.commodity})"
      end
    end

    desc "List the top 25 jobs in descending order by pay per nautical mile (PPNM) where an aircraft exists"
    task list_top_jobs_by_ppnm: :environment do
      results = Job
        .joins(:aircraft)
        .where(aircrafts: { needsrepair: false })
        .select('"jobs"."location", toicao, distance, SUM(pay) as sum_total_pay, SUM(ppnm) as sum_total_ppnm, SUM(amount) as sum_total_amount, unittype, commodity, "aircrafts"."id" as aircraft_id')
        .having('sum_total_pay > ?', 10000)
        .having('unittype = ? AND sum_total_amount <= ?', 'passengers', 19)
        .group(['location', 'toicao', 'unittype', 'aircraft_id'])
        .order('sum_total_ppnm DESC')
        .limit(25)

      results.each do |result|
        puts "#{result.location} -> #{result.toicao} (#{result.distance}nm): $#{result.sum_total_ppnm || result.ppnm} ($#{result.sum_total_pay || result.pay}) to transport #{result.sum_total_amount || result.amount} #{result.unittype} (#{result.commodity})"
      end
    end

    desc "List the top 25 flights in descending order by profit where an aircraft exists"
    task list_top_flights_by_profit: :environment do
      results = Flight
        .joins(:aircraft)
        .where(aircrafts: { needsrepair: false })
        .order('profit DESC')
        .limit(25)

      results.each do |result|
        puts "#{result.from_icao} -> #{result.to_icao} (#{result.distance}nm): $#{result.profit} ($#{result.profit_pnm} per nm) to transport #{result.passenger_count} #{result.passenger_description || 'passengers'} and #{result.cargo_weight}kg of #{result.cargo_description || 'cargo'}"
      end
    end

    desc "Download list of FBOs for sale. Only useful for pricing FBOs. Not the same as refresh_fbos_by_oldest."
    task download_fbos_for_sale: :environment do
      FixedBaseOperator.download_and_save_for_sale
    end

    desc "Destroy old FBO data for an airport and download new FBO data"
    task refresh_fbos_by_oldest: :environment do
      FixedBaseOperator.refresh_oldest_airport
    end

    desc "Creates new flights from newly-fetched jobs given a batch of airports, replacing existing flights"
    task refresh_flights_and_jobs_by_highest_profit: :environment do

      raise RuntimeError, 'No airports found.' unless Airport.count > 0

      beginning_time = Time.now

      result_counts = Flight.refresh_by_highest_profit
      jobs_inserted = result_counts[:jobs_inserted]
      flights_inserted = result_counts[:flights_inserted]

      end_time = Time.now
      elapsed_time = end_time - beginning_time

      inserts_per_second = ((jobs_inserted + flights_inserted) / elapsed_time).round(2)

      puts "#{jobs_inserted} jobs and #{flights_inserted} flights saved at #{inserts_per_second} inserts per second"
    end
end
