require 'test_helper'

class AircraftTypeTest < ActiveSupport::TestCase

  test "method refresh exists" do
    assert AircraftType.respond_to? :refresh
  end

  test "method refresh makes a request to the FSE API" do
    fse_api = MiniTest::Mock.new
    fse_api.expect(:make_request, "<xml></xml>", [{:query => "aircraft", :search => "configs"}])

    AircraftType.refresh(fse_api)
  end
end
