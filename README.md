# fsjobs
A job finder for [FSEconomy](http://www.fseconomy.net/)

It finds the most profitable jobs (assignments) in order to help pilots plan the
most cost effective flights. A cost effective flight is one that maximizes
the amount of money earned per nautical mile flown.

## Running the finder

A lot work (including some network requests to fseconomy.net) is done inside of the FSJobFinder constructor method, so please give the object some time to instantiate.

```
$ irb -r './FSJobFinder.rb'
> fs = FSJobFinder.new
> flights = fs.create_flight_list(fs.jobs_table)
> puts flights.select { |item| item.aircraft > 0 }
```

## Gather the data manually

Gathering job data requires assembling a list of airport ICAOs. This list can then be used in a request like this one:

```
http://server.fseconomy.net/data?userkey={USER_KEY}&format=xml&query=icao&search=jobsfrom&icaos=CZFA-CEX4-CYMA
```

An FSJobFinder object can assemble this list for you. Begin by opening an IRB session with `irb -r './FSJobFinder.rb'`:

```
2.2.2 :001 > finder = FSJobFinder.new
 => #<FSJobFinder:0x007fcb2c824908 @airports_table=#<CSV::Table mode:col_or_row row_count:23761>, @jobs_table=#<CSV::Table mode:col_or_row row_count:61>>
2.2.2 :002 > finder.get_icaos("Hawaii").join('-')
 => "92Z-HI01-HI02-HI03-HI05-HI13-HI23-HI25-HI27-HI28-HI29-HI31-HI32-HI33-HI46-HI49-HI99-JHM-NPS-PAK-PHBK-PHDH-PHFS-PHHI-PHHN-PHJR-PHKO-PHLI-PHLU-PHMK-PHMU-PHNG-PHNL-PHNY-PHOG-PHSF-PHTO-PHUP-XBEL-Z29G"
```
